/**
 * Class FontExplosionAddon
 * Wichtig um Addons zu entwickeln
 */
export default class FontExplosionAddon {
    constructor(input) {
        this.input = input;
        this.updateEnabled();
    }

    /**
     * Methode applyRandom()
     * Wendet irgendwas auf den part an
     * @param part Div Element
     */
    applyRandom(part) {
        return part;
    }

    getEnabled() {
        return this.enabled;
    }

    updateEnabled() {
        this.enabled = this.input.checked;
    }
}
