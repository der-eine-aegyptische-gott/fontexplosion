/**
 * Class Collision
 * Regelt die Collision von Textbausteinen
 */
export default class Collision {
    constructor($container) {
        this.container = $container;
    }

    checkfForCollison(node) {
        let containerCords = this.getBoundingCords(this.container);
        let nodeCords = this.getBoundingCords(node);

        if (nodeCords.top < containerCords.top) {
            console.log(`Top Collision |- ${node.innerHTML} - |`);
            return true;
        }

        //console.log(`left`)

        if (nodeCords.right > containerCords.right) {
            console.log(`Right Collision |- ${node.innerHTML} - |`);
            return true;
        }

        if (nodeCords.bottom > containerCords.bottom) {
            console.log(`Bottom Collision |- ${node.innerHTML} -|`);
            return true;
        }
    }

    getBoundingCords(node) {
        return node.getBoundingClientRect();
    }
}
