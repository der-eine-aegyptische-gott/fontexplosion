import Collision from "./collision";
import * as htmlToImage from "html-to-image";

/**
 * Class FontExplosionCore
 * Der eigentliche Kern des Projektes
 * @param container_id Die Id des Container Objects (Dort wo es dargestellt wird)
 * @param input_id Die Id des Input Objects (Dort wo der Text herkommt)
 * @param play_button_id Die Id des Play Buttons
 * @param reset_button_id Die Id des Reset Buttons
 * @param save_button_id Die Id des Save Buttons
 * @param config Das Config Objekt. Es besteht aus Configs die bereits als Default in der Klasse definiert sind
 * @param addons Externe Addons, die den Kern erweitern
 */
export default class FontExplosionCore {
    textValue;
    // Das Default Config Objekt
    config = {
        textSperator: " ",
        allowOutOfBounce: false,
        customSplit: {
            valueid: "",
            id: "",
            active: false,
        },
        randomizeSingleLetters: false,
    };
    constructor(
        container_id,
        input_id,
        play_button_id,
        reset_button_id,
        save_button_id,
        custom_container_box_id,
        config,
        addons
    ) {
        // Container
        this.container = document.getElementById(container_id);
        if (!this.container) {
            throw "Container wurd nicht gefunden";
        }
        this.container.style.position = "relative";
        this.container.style.overflow = "hidden";
        // Input
        this.input = input_id;

        this.updateTextValue();
        // Play Button
        this.playButton = document.getElementById(play_button_id);
        if (this.playButton) {
            this.playButton.onclick = this.runApp.bind(this);
        }
        // Reset Button
        this.resetButton = document.getElementById(reset_button_id);
        if (this.resetButton) {
            this.resetButton.onclick = this.resetScene.bind(this);
        }
        // Save Button
        this.saveButton = document.getElementById(save_button_id);
        if (this.saveButton) {
            this.saveButton.onclick = this.saveImage.bind(this);
        }

        this.customContainerColorInput = document.getElementById(
            custom_container_box_id
        );
        if (this.customContainerColorInput) {
            this.customContainerColorInput.onchange = this.customBoxColor.bind(
                this
            );
        }

        // Config
        this.config_ids = config;
        // Init Config Object
        // Addons
        this.addons = addons;
    }

    /**
     * Methode runApp()
     * Wird ausgeführt wenn der User auf Play drückt
     */
    runApp() {
        this.updateAddonEnables();
        this.updateConfig();
        this.updateTextValue();
        this.splitIntoDivs();
    }

    /**
     * Methode updateTextValue()
     * Updatet den Text mit der Value des Input Feldes
     */
    updateTextValue() {
        this.textValue = document.getElementById(this.input).value;
        if (!this.textValue) {
            this.textValue =
                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
        }
    }

    /**
     * Methode updateAddonEnables()
     * Updatet den internen Boolean sätmliche Addons
     */
    updateAddonEnables() {
        this.addons.forEach((addon) => addon.updateEnabled());
    }

    /**
     * Methode updateConfig()
     * Updatet die internen Configs des Kerns
     */
    updateConfig() {
        // textSperator
        let textSeperatorElements = document.getElementsByName(
            this.config_ids.textSperator
        );
        if (textSeperatorElements) {
            for (let radio of textSeperatorElements) {
                if (
                    radio.checked &&
                    radio.id === this.config_ids.customSplit.id
                ) {
                    let customvalue = document.getElementById(
                        this.config_ids.customSplit.valueid
                    );
                    if (customvalue) {
                        this.config.textSperator = customvalue.value;
                        this.config.customSplit.active = true;
                    } else {
                        console.log(
                            "Irgendwas ist schiefgelaufen, bitte gucke nochmal nach ob die IDs korrekt sind"
                        );
                    }
                } else if (radio.checked) {
                    this.config.textSperator = radio.value;
                }
            }
        }

        // AllowOutOfBounce
        this.config.allowOutOfBounce = document.getElementById(
            this.config_ids.allowOutOfBounce
        ).checked;
        if (!this.config.allowOutOfBounce) {
            this.collisionChecker = new Collision(this.container);
        } else {
            this.collisionChecker = null;
        }

        this.config.randomizeSingleLetters = document.getElementById(
            this.config_ids.randomizeSingleLetters
        ).checked;
    }

    /**
     * Methode splitIntoDivs()
     * Splitet den Text in einzelne Divs und führt dann Methoden darauf aus
     */
    splitIntoDivs() {
        this.textValue
            .split(this.config.textSperator)
            .map((part, index, array) => {
                part = this.removeUnnecessary(part);
                if (part !== " " && part.length !== 0) {
                    if (
                        this.config.customSplit.active &&
                        index !== array.length - 1
                    ) {
                        part += this.config.textSperator;
                    }
                    let div = document.createElement("div");
                    div.draggable = true;
                    div.ondrag = this.onDragXY.bind(this);
                    // div.ondrop = this.onDragXY.bind(this)
                    if (
                        this.config.randomizeSingleLetters &&
                        !(this.config.textSperator === "")
                    ) {
                        part.split("").map((partletter) => {
                            let span = document.createElement("span");
                            span.innerHTML = partletter;
                            div.appendChild(span);
                            this.applyRandom(span);
                            span.style.position = "static";
                            span.style.transform = "rotate(0)";
                        });
                    } else {
                        div.innerHTML = part;
                    }
                    this.container.appendChild(div);
                    this.applyRandom(div);
                    if (this.collisionChecker) {
                        let counter = 0;
                        while (this.collisionChecker.checkfForCollison(div)) {
                            if (counter === 10) {
                                console.error({
                                    message: "Konnte nicht platziert werden",
                                    element: div,
                                    content: part,
                                });
                                return;
                            }
                            this.applyRandom(div);
                            counter++;
                        }
                    }
                }
            });
    }

    /**
     * Methode removeUnnecessary()
     * Entfernt Sonderzeichen, etc.
     */
    removeUnnecessary(part) {
        return part
            .replace(/(\r\n|\n|\r)/gm, " ")
            .replace(/[.,\/#!$%^&*;:{}=\-_`~()]/g, "");
    }

    /**
     * Methode applyRandom()
     * Wendet sämtliche Addons auf einen Part des Textes an
     */
    applyRandom(part) {
        part.style = "";
        this.addons.forEach((addon) =>
            addon.getEnabled() ? addon.applyRandom(part) : part
        );
    }

    /**
     * Methode resetScene()
     * Löscht den Text aus dem Container
     */
    resetScene() {
        this.container.innerText = "";
    }

    /**
     * Methode onDragXY()
     * Brechnet die Position beim Draggen
     */
    onDragXY(event) {
        // console.log(event)
        if (event.clientX > 0 && event.clientY > 0) {
            console.log(this.container.offsetWidth);
            console.log(this.container.offsetHeight);
            console.log(event.clientX);
            console.log(event.clientY);
            event.currentTarget.style.top =
                Math.floor(
                    (event.clientY / this.container.offsetHeight) * 100
                ) + "%";
            event.currentTarget.style.left =
                Math.floor((event.clientX / this.container.offsetWidth) * 100) +
                "%";
        }
        // part.style.top = "50%";
        // part.style.left = "50%";
        // part.className = "transform_part";
        // let { width, height } = this.container.getBoundingClientRect();
        // let transform_x = Math.floor(Math.random() * (width / 1.5)) - width / 3;
        // let transform_y =
        //     Math.floor(Math.random() * (height / 1.5)) - height / 3;
        // part.style.transform += `translate(${transform_x}px , ${transform_y}px)`;
    }

    /**
     * Methode saveImage()
     * Speichert den Container als Bild ab
     */
    saveImage() {
        htmlToImage
            .toPng(document.getElementById("container"), {
                width: document.getElementById("container").clientWidth,
                height: document.getElementById("container").clientHeight,
            })
            .then(function (dataUrl) {
                let link = document.createElement("a");
                link.download = "FontExplosion.png";
                link.href = dataUrl;
                link.click();
            });
    }

    customBoxColor(e) {
        this.container.style.backgroundColor = e.target.value;
    }
}
