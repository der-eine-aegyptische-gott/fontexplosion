import "../styles/style";

// Core
import FontExplosionCore from "./core";

// Addons
import FontExplosionRandomColor from "./addons/randomColor";
import FontExplosionRandomRotate from "./addons/randomRotate";
import FontExplosionRandomPosition from "./addons/randomPosition";
import FontExplosionRandomSize from "./addons/randomSize";
import FontExplosionRandomFonts from "./addons/randomFonts";
import FontExplosionLegibility from "./addons/Legibility/index";
// Javascript/CSS Shit
import FontExplosionWindow from "./utils/window";

const core = new FontExplosionCore(
    "container",
    "fancytext",
    "explosion_button",
    "reset_button",
    "save_button",
    "customContainerColor",
    {
        textSperator: "splitLetter",
        allowOutOfBounce: "outofbounce",
        customSplit: {
            valueid: "customSplitValue",
            id: "splitLetter3",
        },
        randomizeSingleLetters: "randomizeSingleLetters",
    },
    [
        new FontExplosionRandomColor(document.getElementById("randomColor")),
        new FontExplosionRandomRotate(document.getElementById("randomRotate")),
        new FontExplosionRandomPosition(
            document.getElementById("randomPosition")
        ),
        new FontExplosionRandomSize(
            document.getElementById("randomSize"),
            document.getElementById("randomSizeMin"),
            document.getElementById("randomSizeMax")
        ),
        new FontExplosionRandomFonts(document.getElementById("randomFonts")),
        new FontExplosionLegibility(
            document.getElementById("legibility"),
            document.getElementById("container")
        ),
    ]
);

const window = new FontExplosionWindow([
    {
        button: "toggleWindow",
        toogleElement: "window-wrapper",
        beforeText: "Hide Window",
        afterText: "Show Window",
    },
    {
        button: "toggleConfig",
        toogleElement: "config-box",
    },
    {
        button: "toggleTextSeperator",
        toogleElement: "textSeperatorList",
    },
    {
        button: "defaultConfig",
        toogleElement: "defaultConfigList",
    },
    {
        button: "randomSizeLabel",
        toogleElement: "randomSizeNummbers",
    },
    {
        button: "customSplitId",
        toogleElement: "customSplitBox",
        radio: true,
        others: "customSplitOther",
    },
]);
