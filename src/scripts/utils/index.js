export default class Utils {
    static convertHttpUrlToHttps = (url) => {
        return url.replace(/^http:\/\//i, "https://");
    };
}
