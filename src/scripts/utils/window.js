/**
 * Class FontExplosionWindow
 * Gibt Listen, etc. die Animationslogik
 * @param array Array an Objekten mit folgendem Inhalt:
 * button: Der Button
 * toggleElement: Die Liste/Box welches die Klasse visible bekommt/verliert
 * beforeText/afterText: Wichtig, wenn z.B. der Change von Hide/Show umgesetzt werden soll
 */

export default class FontExplosionWindow {
    constructor(array) {
        for (let obj of array) {
            let boxtoggle = document.getElementById(obj.button);
            let windowbox = document.getElementById(obj.toogleElement);
            if (boxtoggle && windowbox) {
                if (obj.radio) {
                    for (let other of document.getElementsByClassName(
                        obj.others
                    )) {
                        other.onclick = () => {
                            windowbox.classList.remove("visible");
                        };
                    }
                    boxtoggle.onclick = () => {
                        windowbox.classList.add("visible");
                        if (obj.beforeText && obj.afterText) {
                            boxtoggle.innerHTML === obj.beforeText
                                ? (boxtoggle.innerHTML = obj.afterText)
                                : (boxtoggle.innerHTML = obj.beforeText);
                        }
                    };
                } else {
                    boxtoggle.onclick = () => {
                        windowbox.classList.toggle("visible");
                        if (obj.beforeText && obj.afterText) {
                            boxtoggle.innerHTML === obj.beforeText
                                ? (boxtoggle.innerHTML = obj.afterText)
                                : (boxtoggle.innerHTML = obj.beforeText);
                        }
                    };
                }
            }
        }
    }
}
