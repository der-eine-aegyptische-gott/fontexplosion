import FontExplosionAddon from "../core/addon";

export default class RandomRotate extends FontExplosionAddon {
    constructor(input) {
        super(input);
    }

    applyRandom(part) {
        part.style.transform +=
            "rotate(" + Math.floor(Math.random() * 100) + "deg)";
        return part;
    }
}
