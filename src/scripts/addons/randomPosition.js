import FontExplosionAddon from "../core/addon";

export default class RandomPosition extends FontExplosionAddon {
    constructor(input) {
        super(input);
    }

    applyRandom(part) {
        part.style.position = "absolute";
        part.style.top = Math.floor(Math.random() * 95) + "%";
        part.style.left = Math.floor(Math.random() * 95) + "%";
        return part;
    }
}
