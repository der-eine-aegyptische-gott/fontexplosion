export default class Noise {
    max_vertices = 256;
    max_vertices_mask;
    amplitude;
    scale;
    r = [];

    constructor(scale, amplitude) {
        for (var i = 0; i < this.max_vertices; ++i) {
            this.r.push(Math.random());
        }
        this.max_vertices_mask = this.max_vertices - 1;
        this.scale = scale;
        this.amplitude = amplitude;
    }

    /**
     * Linear interpolation function.
     * @param a The lower integer value
     * @param b The upper integer value
     * @param t The value between the two
     * @returns {number}
     */
    lerp(a, b, t) {
        return a * (1 - t) + b * t;
    }

    getVal(x) {
        var scaledX = x * this.scale;
        var xFloor = Math.floor(scaledX);
        var t = scaledX - xFloor;
        var tRemapSmoothstep = t * t * (3 - 2 * t);

        /// Modulo using &#038;
        var xMin = xFloor % this.max_vertices_mask;
        var xMax = (xMin + 1) % this.max_vertices_mask;

        var y = this.lerp(this.r[xMin], this.r[xMax], tRemapSmoothstep);

        return y * this.amplitude;
    }
}
