import FontExplosionAddon from "../../core/addon";
import Noise from "./perlin";

// Leserlichkeit
export default class Legibility extends FontExplosionAddon {
    // Noise Objekt
    perlin;
    // Container
    container;
    // Config
    config = {
        // Sollen sich die Parts immer gleich rotieren oder pro Part ein anderer Wert (Wenns true is, is es leicht witzlos)
        stable: false,
        // Scale: Wie weit sich der Wert "dehnt" (Guck euch einfach https://plnkr.co/edit/SnCuRTG5Mkb0676ncYmH an, da versteht ihr was ich mit "dehnen" meine)
        scale: 0.7,
        // Amplitude : Wie weit der ausschlagen soll (Probiert mal einen hohen Wert aus :])
        amplitude: 0.25,
    };
    counter = 1;

    constructor(input, container, config) {
        super(input);
        this.container = container;
        if (config) {
            if (!config.stable) {
                this.config.stable = config.stable;
            }
            if (config.scale && typeof config.scale === "number") {
                this.config.scale = config.scale;
            }
            if (config.amplitude && typeof config.amplitude === "number") {
                this.config.amplitude = config.amplitude;
            }
        }
        console.log(this.config.amplitude);

        this.perlin = new Noise(this.config.scale, this.config.amplitude);
    }

    createLineGrid() {}

    applyRandom(part) {
        if (this.container && this.perlin) {
            part.style.display = "inline-block";
            part.style.padding = "0 10px";
            if (Math.random() * 100 > 50) {
                part.style.transform +=
                    "rotate(" + this.perlin.getVal(this.counter) * 100 + "deg)";
            } else {
                part.style.transform +=
                    "rotate(-" +
                    this.perlin.getVal(this.counter) * 100 +
                    "deg)";
            }
            if (!this.config.stable) {
                this.counter++;
            }
        }
        return part;
    }
}
