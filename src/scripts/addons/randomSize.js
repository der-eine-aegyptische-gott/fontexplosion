import FontExplosionAddon from "../core/addon";

export default class RandomSize extends FontExplosionAddon {
    constructor(input, min, max) {
        super(input);
        this.minId = min;
        this.maxId = max;
        this.updateEnabled();
    }

    applyRandom(part) {
        part.style.fontSize =
            Math.floor(Math.random() * (this.max - this.min)) + this.min + "px";
        return part;
    }

    updateEnabled() {
        super.updateEnabled();
        if (this.minId && this.maxId) {
            this.min = this.minId.value;
            this.max = this.maxId.value;
            this.min = Math.ceil(this.min);
            this.max = Math.floor(this.max);
        }
    }
}
