import FontExplosionAddon from "../core/addon";

export default class RandomColor extends FontExplosionAddon {
    constructor(input) {
        super(input);
    }

    applyRandom(part) {
        part.style.color = this.getRandomColor();
        return part;
    }

    getRandomColor() {
        var letters = "0123456789ABCDEF";
        var color = "#";
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}
