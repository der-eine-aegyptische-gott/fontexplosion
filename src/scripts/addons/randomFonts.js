import axios from "axios";
import FontExplosionAddon from "../core/addon";
import Utils from "../utils";

export default class RandomFonts extends FontExplosionAddon {
    constructor(input) {
        super(input);
        this.fonts = [];
        this.getGoogleFonts().then((data) => {
            this.prepareFonts(data);
        });
    }

    applyRandom(part) {
        part.style.fontFamily = this.getRandomFont().family;
        return part;
    }

    getGoogleFonts() {
        return axios
            .get(
                "https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyA88iL3YOGzr0gcK2Jb2fU-vtacU1zMggc"
            )
            .then(function (response) {
                return response.data.items;
            })
            .catch(function (error) {
                console.log("fetching fonts didnt work: " + error);
            });
    }

    getRandomFont() {
        let length = 0;
        if (this.fonts.length !== 0) {
            length = this.fonts.length - 1;
        }
        let rng = Math.floor(Math.random() * length);
        return this.fonts[rng];
    }

    async prepareFonts(fontArray) {
        const fontLimit = 20;
        let fontLoaded = 0;
        for (const e of fontArray) {
            if (e.family) {
                if (fontLoaded === fontLimit) {
                    return;
                }
                if (e.files.regular) {
                    let newFont = new FontFace(
                        e.family,
                        `url(${Utils.convertHttpUrlToHttps(e.files.regular)})`
                    );
                    this.fonts.push({
                        family: e.family,
                    });
                    let newStyle = document.createElement("style");
                    newStyle.appendChild(
                        document.createTextNode(
                            "\
                    @font-face {\
                        font-family: " +
                                e.family +
                                ";\
                        src: url('" +
                                Utils.convertHttpUrlToHttps(e.files.regular) +
                                "') ;\
                    }\
                    "
                        )
                    );
                    document.head.appendChild(newStyle);
                    await newFont.load();
                    document.fonts.add(newFont);
                    fontLoaded++;
                }
            }
        }
    }
}
